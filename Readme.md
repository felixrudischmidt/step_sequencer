# Step Sequencer
Dies ist das Repository für das Hauptprojekt im Modul Audio/Video von Felix Schmidt, Jonas Klein und Jan Börner. 
In dem Projekt wurde ein Step Sequencer umgesetzt. Der Step Sequencer enthält folgende Features:

* MIDI-Learn von Slider Komponenten für das generische Einbinden von MIDI-Controllern

* Eine 8×8 Matrix um Beats zu programmieren

* Einen BPM-Slider für das Anpassen des Tempos

* Einen Slider zum Anpassen der Playback Rate 

* Die Möglichkeit eigene Samples einzubinden

* Eine Vorbelegung mit Standard Samples

## Verwendete Technologien
Für die Umsetzung der Applikation wurden zum einen die Web Audio API sowie VueJs verwendet. Des Weiteren wird JQuery sowie Font-Awesome verwendet.

## Installation
Für die Installation der Applikation wird node und npm benötigt. Um die Installation zu starten, tippen sie folgendes Kommando im Root Verzeichnis des Projektordners:

``$ npm install``

## Starten der Applikation
Um die Applikation zu starten, tippen sie folgendes Kommando:

``$ npm run start``

## Bedienung der Benutzeroberfläche
![](screenshot.png)
In der oberen Abbildung ist die Benutzeroberfläche des Step Sequencer abgebildet.

### Beat Matrix
In der Beat Matrix können durch Klicken auf eine Kachel Trigger Points gesetzt werden. 
Wenn der Sequencer läuft, wird der jeweilige Sample abgespielt. Durch erneutes Klicken, wird der Trigger Point wieder gelöscht.

### MIDI-Learn
Durch Klicken auf den MIDI Learn Button in der linken oberen Hälfte der Benutzeroberfläche kann ein MIDI-Controller mit einem Slider in der Benutzeroberfläche verbunden werden. 
Klicken Sie zuerst auf MIDI-Learn, danach auf den Slider den Sie mappen möchten. Jetzt bewegen Sie einen Knopf an ihrem MIDI-Controller und klicken danach wieder auf den 
MIDI-Learn Button. Der Slider auf dem Controller und der Slider in der Benutzeroberfläche sind jetzt verbunden.

### Eigene Samples Hinzufügen
Um eigene Samples zu einer Spur hinzuzufügen, können Sie einen Sample von Ihrem Computer direkt auf eine beliebige Spur laden. Bewegen Sie dazu die Sampledatei per Drag-and-drop 
auf den Button links in jeder Spur.  
Auf diese Option werden Sie beim Hovern mit der Maus über dem Button hingewiesen. Klicken Sie auf diesen Button, um den Namen der Spur zu bearbeiten. 

### Pattern Selector
Mit dem Pattern Selector können Patterns gespeichert werden. Wenn sie ein Beat Pattern in der Beat Matrix programmiert haben, können Sie das Pattern durch einen Klick auf
das Speichern Icon speichern. Im Pattern Selector auf der rechten Seite der Benutzeroberfläche erscheinen die gespeicherten Patterns. Mit klick auf ein Pattern wird
ein Pattern in die Beat Matrix geladen. Mit dem Stift Icon können Sie bestehende Patterns bearbeiten. 