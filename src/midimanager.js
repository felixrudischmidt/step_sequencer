if (navigator.requestMIDIAccess) {
	const controlerUIMap = new Map();
	var deviceToLearn = undefined;
	var learnMode = false;

	function onMIDISuccess(midiAccess) {
		console.log(midiAccess);
		for (var input of midiAccess.inputs.values())
			input.onmidimessage = getMIDIMessage;
	}

	function getMIDIMessage(midiMessage) {

		function scaleValue(min, max, value) {
			let amount = value / 127;
			let range;

			if (min >= 0) {
				range = max - min;
			} else {
				range = max + min;
			}

			let v;
			if (range > 10) {
				v = Math.round(amount * range);
			} else {
				v = amount * range;
			}
			let q = v + min;
			return q;
		}

		if (learnMode) {
			let controlerId = midiMessage.data[0].toString() + midiMessage.data[1].toString();
			if (deviceToLearn === undefined) {
				return;
			}
			controlerUIMap.set(controlerId, deviceToLearn);
			return;
		}

		let element = controlerUIMap.get(midiMessage.data[0].toString() + midiMessage.data[1].toString());
		if (element !== undefined) {
			let uiComponent = $(`#${element}`);
			let scaledValue = scaleValue(parseInt(uiComponent.attr("min")), parseInt(uiComponent.attr("max")), midiMessage.data[2]);
			app[element] = scaledValue;
		}
	}

	function onMIDIFailure() {
		console.log('Could not access your MIDI devices.');
	}

	function toogleLearning(t) {
		if (learnMode) {
			alert("Enter playing mode");
			learnMode = false;
		} else {
			alert("Enter learning mode");
			learnMode = true;
		}
	}

	function learnAble(t) {
		if (learnMode) {
			deviceToLearn = t.id;
		} else {
			deviceToLearn = undefined;
		}
	}

	navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
} else {
	alert('WebMIDI is not supported in this browser.');
}