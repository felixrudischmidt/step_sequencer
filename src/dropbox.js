import FileHandler from './fileHandler.js';
import Singleton from './singleton.js';

/**
 * class to handle events of dropbox button in every track like dragging and dropping a 
 * sound file there or naming the track
 */
export default class Dropbox {

    constructor(id, audioCtx) {
        this.singleton = new Singleton(audioCtx);

        this.element = document.getElementById(id);
        this.element.addEventListener("dragenter", this.dragenter.bind(this), false);
        this.element.addEventListener("dragover", this.dragover.bind(this), false);
        this.element.addEventListener("drop", this.drop.bind(this), false);
        this.element.addEventListener("click", this.click.bind(this), false);
        this.element.addEventListener("mouseover", this.mouseover.bind(this), false);
        this.element.addEventListener("mouseout", this.mouseout.bind(this), false);

        this.id = id;
        this.tmpLabel = "";
    }

    // nothing happens
    dragenter(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    // nothing happens
    dragover(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    // load an audio file via drag-and-drop
    drop(e) {
        e.stopPropagation();
        e.preventDefault();

        const dt = e.dataTransfer;
        const files = dt.files;

        this.singleton.fileHandler.handleFiles(files, this);
    }

    // click dropbox button to name track
    click(e) {
        let label = prompt("Please name this track.");
        if (label !== "" && label !== null) {
            this.updateLabel(label)
        }
    }

    // button text changes to help dialog to notify user of drag-and-drop capability
    mouseover(e) {
        this.tmpLabel = this.element.innerText
        this.element.innerText = "Drag and drop sound files here.";
    }

    // help text is only visible when hovering over button
    mouseout(e) {
        this.element.innerText = this.tmpLabel;
    }

    // helper function to update button text
    updateLabel(label) {
        this.tmpLabel = label;
        this.element.innerText = label;
    }
}
