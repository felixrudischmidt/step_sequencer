import FileHandler from './fileHandler.js';
import Player from './player.js';
import Scheduler from './scheduler.js';

import Singleton from './singleton.js';
import Dropbox from './dropbox.js';
import PatternSelector from './patternselector.js';

/**
* main class that initiates the Step Sequencer with its pads, dropboxes, 
* Pattern Selecter and loadsdefault samples
*/

const audioCtx = new AudioContext();
const singleton = new Singleton(audioCtx);

let player = singleton.player;
let fileHandler = singleton.fileHandler;
let scheduler = singleton.scheduler;

// dropbox buttons for adding user input samples and naming tracks
let dropbox1 = new Dropbox("dropbox-1", audioCtx);
let dropbox2 = new Dropbox("dropbox-2", audioCtx);
let dropbox3 = new Dropbox("dropbox-3", audioCtx);
let dropbox4 = new Dropbox("dropbox-4", audioCtx);
let dropbox5 = new Dropbox("dropbox-5", audioCtx);
let dropbox6 = new Dropbox("dropbox-6", audioCtx);
let dropbox7 = new Dropbox("dropbox-7", audioCtx);
let dropbox8 = new Dropbox("dropbox-8", audioCtx);

// grab checkboxes from the interface
// group them by rows, i.e. tracks

const allPadButtons = document.querySelectorAll('#tracks .pads button');

// Patternselector
let patternSelector = new PatternSelector(allPadButtons, document.querySelector('#patternList'), document.querySelector('#patternEdit'), document.querySelector('#addpattern'));

// switch aria attribute on click
allPadButtons.forEach(el => {
    el.addEventListener('click', () => {
        if (el.getAttribute('aria-checked') === 'false') {
            el.setAttribute('aria-checked', 'true');
        } else {
            el.setAttribute('aria-checked', 'false');
        }
    }, false)
});

// load default samples
fileHandler.setupSample('public/Audacity_Kick.wav', audioCtx)
    .then((sample) => {
        fileHandler.loadingEl.style.display = 'none';
        player.sample_1 = sample; // to be used in player by playSample function
        player.playButton.addEventListener('click', player.eventHandler.bind(player))
    });

fileHandler.setupSample('public/Audacity_Bell.wav', audioCtx)
    .then((sample) => {
        fileHandler.loadingEl.style.display = 'none';
        player.sample_2 = sample;
    });

fileHandler.setupSample('public/Kick.wav', audioCtx)
    .then((sample) => {
        fileHandler.loadingEl.style.display = 'none';
        player.sample_3 = sample;
    });

fileHandler.setupSample('public/Closed_Hat.wav', audioCtx)
    .then((sample) => {
        fileHandler.loadingEl.style.display = 'none';
        player.sample_4 = sample;
    });

fileHandler.setupSample('public/Sample_1.wav', audioCtx)
    .then((sample) => {
        fileHandler.loadingEl.style.display = 'none';
        player.sample_5 = sample;
    });

fileHandler.setupSample('public/Bassdrum.wav', audioCtx)
    .then((sample) => {
        fileHandler.loadingEl.style.display = 'none';
        player.sample_6 = sample;
    });

fileHandler.setupSample('public/Akkordeon1.wav', audioCtx)
    .then((sample) => {
        fileHandler.loadingEl.style.display = 'none';
        player.sample_7 = sample;
    });

fileHandler.setupSample('public/Akkordeon2.wav', audioCtx)
    .then((sample) => {
        fileHandler.loadingEl.style.display = 'none';
        player.sample_8 = sample;
    });