import PlayerSingleton from './player.js';
import FileHandler from './fileHandler.js';
import Scheduler from './scheduler.js';
import Player from './player.js';

let singleton = null;
/**
* singleton class to access most important classes from anywhere
*/
class Singleton {

    constructor(audioCtx) {

        if (!singleton) {
            singleton = this;
            this.player = new Player(audioCtx);
            this.fileHandler = new FileHandler(audioCtx);
            this.scheduler = new Scheduler(audioCtx);
        }
        return singleton;
    }

    getScheduler() {
        return this.scheduler;
    }

}

export default Singleton;
