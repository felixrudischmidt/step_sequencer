/**
 * Ermoeglicht das Speichern, Laden und Bearbeiten von Pattern
 */
export default class PatternSelector {

    /**
     * Konstruktor, Erstellt vom Patternselector ein Objekt
     * @param {domElement} pads Legt die Pads fest, auf die sich der Patternselector bezieht
     * @param {domElement} domPatternList Container-Element, in welchem die Patternliste angezeigt wird
     * @param {domElement} domPatternEditor Container-Element, in welchem sich das Editor-Feld befinden wird
     * @param {domElement} domAddPatternBtn Dom-Element welches bei Klick das Editor-Feld schließen wird
     */
    constructor(pads, domPatternList, domPatternEditor, domAddPatternBtn) {
        this.pads = pads;
        this.domPatternList = domPatternList;
        this.domPatternEditor = domPatternEditor;

        this.patternList = new Array();
        this.selectedIdx = 0;
        this.editID = 0;

        this.patternEditTitle = domPatternEditor.querySelector('.editTitle span');

        this.patternListEntryTemplate = document.createElement('template');
        this.patternListEntryTemplate.innerHTML = '<div class="pattern" id="patternX"><span class="patternName"></span><span class="patternEdit"><i class="fa fa-pencil" aria-hidden="true"></i></span><span class="patternRemove"><i class="fa fa-trash-o" aria-hidden="true"></i></span></div>';

        // Auf Keypress fuer Patternauswahl lauschen
        document.addEventListener('keydown', this.keyPress.bind(this));
        domPatternEditor.querySelector('.closebtn').addEventListener("click", this.closePatternEditor.bind(this));

        // Pads im Patternselector hinzufuegen
        let html = '';
        pads.forEach((e) => {
            if (e.parentNode.firstElementChild == e) {
                html += '<div class="row">';
            }
            html += '<div class="pad" aria-checked="false"></div>';
            if (e.parentNode.lastElementChild == e) {
                html += '</div>';
            }
        });

        // Pads im Patternselector mit KlickListener versorgen
        domPatternEditor.querySelector('.editPads').innerHTML = html;
        this.editPads = domPatternEditor.querySelectorAll('.pad');
        this.editPads.forEach((e) => {
            e.addEventListener("click", this.editPadClick.bind(this));
        });

        // Eventlistener Speicher-Button
        domAddPatternBtn.addEventListener("click", this.addPattern.bind(this));

        // Selektiere PatternEdit-Textfeld bei Klick
        // https://stackoverflow.com/questions/1173194/select-all-div-text-with-single-mouse-click/20079910#20079910
        this.patternEditTitle.addEventListener('click', function (e) {
            window.getSelection().selectAllChildren(this.patternEditTitle);
        }.bind(this));
        // Speichere neuen Namen
        this.patternEditTitle.addEventListener('keyup', function (e) {
            this.domPatternList.querySelector('#pattern_' + this.editID + ' span').innerHTML = this.patternEditTitle.innerHTML;
        }.bind(this));
    }

    /**
     * Callback wenn ein Pad zur editierung eines Patterns angeklickt wurde
     */
    editPadClick(e) {
        if (e.target.getAttribute('aria-checked') == 'true') {
            e.target.setAttribute('aria-checked', 'false');
        } else {
            e.target.setAttribute('aria-checked', 'true');
        }
        this.updatePattern(this.editID, this.editPads);
    }

    /**
     * Schlieszt den PatternEditor
     */
    closePatternEditor() {
        this.domPatternEditor.style.display = 'none';
    }

    /**
     * Callback-Function die bei KeyPress ausgefuehrt wird
     * Falls Ziffer und EditPattern geschlossen -> Pattern Laden
     * Falls Esc und EditPattern geoeffnet -> EditPattern schließen
     * @param {keydown event} e
     */
    keyPress(e) {
        console.log(e.keyCode);
        if (this.domPatternEditor.style.display == 'none') {
            //0-9, 0-9(NumBlock)
            if (e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode >= 96 && e.keyCode <= 105) {
                var id = this.domPatternList.children[e.key].getAttribute('id').substr(8)
                this.loadPattern(id, this.pads);
            }
        } else {
            //Esc
            if (e.keyCode == 27) {
                this.closePatternEditor();
            }
        }
    }

    /**
     * Fuegt aktuelles Pattern den Patterns hinzu
     * @returns {int} Id, unter dem das Pattern gespeichert wurde
     */
    addPattern(e) {
        let newEntry = this.patternListEntryTemplate.content.cloneNode(true);
        let id = this.selectedIdx++;
        newEntry.querySelector('.patternName').innerHTML = this.tierarten[id];
        newEntry.children[0].setAttribute('id', "pattern_" + id);

        this.patternList[id] = Array();
        for (var i = 0; i < this.pads.length; i++) {
            this.patternList[id][i] = this.pads[i].getAttribute('aria-checked');
        }

        newEntry.querySelector('.patternName').addEventListener('click', function (e) {
            this.loadPattern(e.target.parentNode.getAttribute('id').substr(8), this.pads);
        }.bind(this));
        newEntry.querySelector('.patternEdit').addEventListener('click', function (e) {
            this.loadPattern(e.target.parentNode.parentNode.getAttribute('id').substr(8), this.editPads);
            this.editID = e.target.parentNode.parentNode.getAttribute('id').substr(8);
            this.domPatternEditor.style.display = 'block';
            this.patternEditTitle.innerHTML = e.target.parentNode.parentNode.firstChild.innerHTML;
        }.bind(this));
        newEntry.querySelector('.patternRemove').addEventListener('click', function (e) {
            this.removePattern(e.target.parentNode.parentNode.getAttribute('id').substr(8));
        }.bind(this));

        this.domPatternList.appendChild(newEntry);
        return id;
    }

    /**
     * Aktualisiert ein pattern
     * @param {int} id zu loeschender Index
     * @param {domElement} srcPads Datenquelle
     */
    updatePattern(id, srcPads) {
        for (var i = 0; i < srcPads.length; i++) {
            this.patternList[id][i] = srcPads[i].getAttribute('aria-checked');
        }
    }

    /**
     * Loescht ein gespeichertes Pattern anhand einer ID
     * @param {int} Index zu loeschender Index
     */
    removePattern(id) {
        this.domPatternList.querySelector('#pattern_' + id).remove();
        delete patternList[id];
    }

    /**
     * Laedt ein Pattern anhand einer ID
     * @param {int} Index zu ladender Index
     */
    loadPattern(id, targetPads) {
        for (var i = 0; i < targetPads.length; i++) {
            targetPads[i].setAttribute('aria-checked', this.patternList[id][i]);
        }
    }

    /**
     * @var {String[]} Namensliste für Pattern
     */
    tierarten = [
        "Warzenschwein",
        "Paletten-Doktorfisch",
        "Schwarzer Schwan",
        "Chinesischer Muntjak",
        "Schnepfenmesserfisch",
        "Kardinalbarsch",
        "Teichfrosch",
        "Giraffe",
        "Kapuziner",
        "Erdmännchen",
        "Goldkatze",
        "Trampeltier",
        "Anemonenfisch",
        "Asiatischer Elefantenbulle",
        "Sekretär",
        "Goldzügelbülbül",
        "Purpurtangare",
        "Kugelgürteltier",
        "Fidschileguan",
        "Guereza",
        "Sulawesi Erdschildkröte",
        "Springbock",
        "Webervogel",
        "Flamingo",
        "Gnu",
        "Würfelnatter",
        "Gänsegeier",
        "Springtamarin",
        "Einsiedlerkrebs",
        "Kaiman",
        "Dohle",
        "Graureiher",
        "Dunkle Tigerpython",
        "Zwergesel",
        "Sonnenvogel",
        "Löwe",
        "Strauß",
        "Islandpferd",
        "Ameisenbär",
        "Breitmaulnashorn",
        "Gelbbrust-Kapuziner",
        "Königspython",
        "Urwildpferd",
        "Rothaubenturako",
        "Schlangenhalsschildkröte",
        "Kronenkranich",
        "Weißstorch",
        "Zwergsäger",
        "Sternschildkröte",
        "Wasserbock",
        "Blattschneiderameise",
        "Mandarinfisch",
        "Sumpfmeerschweinchen",
        "Faultier",
        "Zwerggleitbeutler",
        "Shetlandpony",
        "Muntjak",
        "Stelzenläufer",
        "Elster",
        "Straußwachtel",
        "Oryxweber",
        "Mehlwürmer",
        "Rotschulterente",
        "Schwarzstorch",
        "Prinz-Alfred Hirsch",
        "Nashornleguan",
        "Seepferdchen",
        "Kea",
        "Strumpfbandnatter",
        "Segelflossen-Doktorfisch",
        "Halsbandpekari",
        "Röhrenaal",
        "Zhous Scharnierschildkröte",
        "Koi",
        "Pacu",
        "Posavina Pferd",
        "Tiger",
        "Gila-Krustenechse",
        "Nilflughund",
        "Stachelschwanzskink",
        "Kaffern-Hornrabe",
        "Rotscheitelmangabe",
        "Orang-Utan",
        "Säbelschnäbler",
        "Gorilla",
        "Grüne Baumeidechse",
        "Stachelschwein",
        "Kurzohr-Rüsselspringer",
        "Vogelspinne",
        "Putzerlippfisch",
        "Eisvogel",
        "Tigerpython",
        "Kaiser- und Rothalsgans",
        "Scharnierschildkröte",
        "Grünarassari",
        "Balistar",
        "Kampfläufer",
        "Mönchsgeier",
        "Kegelrobbe",
        "Eichhörnchen",
        "Wolf",
        "Pfeilgiftfrosch",
        "Gepard",
        "Malaienhuhn",
        "Bartagame",
        "Nasenbär",
        "Süßwassernadel",
        "Türkisblauer Zwergtaggecko",
        "Genickbandweber",
        "Zwergziege",
        "Lachender Hans",
        "Diademseeigel",
        "Türkei-Stachelmaus",
        "Goldenes Löwenäffchen",
        "Mandschurenkranich",
        "Zweifarbtamarin",
        "Heimchen",
        "Zwergotter",
        "Ouessantschaf",
        "Gebirgslori",
        "Elenantilope",
        "Riesenstabschrecke",
        "Bongo",
        "Austernfischer",
        "Pelikan",
        "Brahma-Huhn",
        "Pagodenstar",
        "Gaur",
        "Bartkauz",
        "Asiatische Elefantenkuh",
        "Stockente",
        "Fasantauben",
        "Katta",
        "Buntspecht",
        "Zwergmaus",
        "Pinguin",
        "Dülmener Pony",
        "Süßwasser-Stechrochen",
        "Zwergseidenäffchen",
        "Mongolenpony",
        "Hausmeerschweinchen",
        "Spatz",
        "Vari",
        "Leopard",
        "Krauskopfpelikan",
        "Zebra",
        "Malaienbär",
        "Gürteltier",
        "Flughund",
        "Annam-Bachschildkröte"];
}