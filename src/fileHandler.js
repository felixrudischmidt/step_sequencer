import Singleton from './singleton.js';

/*
* class that deals with file operations like loading default and new user input samples
*/
export default class FileHandler {

    constructor(audioCtx) {
        this.audioCtx = audioCtx;
        this.singleton = new Singleton(audioCtx);
        this.loadingEl = document.querySelector('.loading');
    }

    // deal with sound file that was handed over from dropbox
    async handleFiles(files, dropbox) {
        debugger;
        let file = files[0];
        dropbox.updateLabel(files[0].name);
        let sampleBuffer = await file.arrayBuffer();
        let audioBuffer = await this.audioCtx.decodeAudioData(sampleBuffer);

        switch (dropbox.id) {
            case "dropbox-1":
                this.singleton.player.sample_1 = audioBuffer;
                break;
            case "dropbox-2":
                this.singleton.player.sample_2 = audioBuffer;
                break;
            case "dropbox-3":
                this.singleton.player.sample_3 = audioBuffer;
                break;
            case "dropbox-4":
                this.singleton.player.sample_4 = audioBuffer;
                break;
            case "dropbox-5":
                this.singleton.player.sample_5 = audioBuffer;
                break;
            case "dropbox-6":
                this.singleton.player.sample_6 = audioBuffer;
                break;
            case "dropbox-7":
                this.singleton.player.sample_7 = audioBuffer;
                break;
            case "dropbox-8":
                this.singleton.player.sample_8 = audioBuffer;
                break;
            default:
        }
    }

    // fetch the audio file of default sample and decode the data
    async getFile(audioContext, filepath) {
        const response = await fetch(filepath);
        const arrayBuffer = await response.arrayBuffer();
        const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
        return audioBuffer;
    }

    // Here we're `await`ing the async/promise that is `getFile`.
    // To be able to use this keyword we need to be within an `async` function
    async setupSample(filePath, audioCtx) {
        const sample = await this.getFile(audioCtx, filePath);
        return sample;
    }
}

