import Player from './player.js';
import Singleton from './singleton.js';

/*
* class that manages scheduling tasks, i.e. when the sound head is moving along the grid, the 
* activated notes are played
*/
export default class Scheduler {

    constructor(audioCtx) {
        this.audioCtx = audioCtx;
        this.singleton = new Singleton(audioCtx);

        this.tempo = app.bpm;
        this.bpmControl = document.querySelector('#bpm');
        this.bpmValEl = document.querySelector('#bpmval');

        this.bpmControl.addEventListener('input', ev => {
            this.tempo = Number(ev.target.value);
            this.bpmValEl.innerText = this.tempo;
        }, false);

        this.lookahead = 25.0; // How frequently to call scheduling function (in milliseconds)
        this.scheduleAheadTime = 0.1; // How far ahead to schedule audio (sec)
        this.currentNote = 0; // The note we are currently playing
        this.nextNoteTime = 0.0; // when the next note is due.

        // Create a queue for the notes that are to be played, with the current time that we want them to play:
        this.notesInQueue = [];

        this.pads = document.querySelectorAll('.pads');
        this.timerID = null;
        this.lastNoteDrawn = 3;

    }

    nextNote() {
        const secondsPerBeat = 60.0 / Number(app.bpm);//this.tempo;
        this.nextNoteTime += secondsPerBeat; // Add beat length to last beat time
        // Advance the beat number, wrap to zero
        this.currentNote++;
        if (this.currentNote === 8) {
            this.currentNote = 0;
        }
    }

    scheduleNote(beatNumber, time) {
        // push the note on the queue, even if we're not playing.
        this.notesInQueue.push({
            note: beatNumber,
            time: time
        });
        // console.log(beatNumber, time);
        if (this.pads[0].querySelectorAll('button')[this.currentNote].getAttribute('aria-checked') === 'true') {
            this.singleton.player.playSample(this.audioCtx, this.singleton.player.sample_1);
        }
        if (this.pads[1].querySelectorAll('button')[this.currentNote].getAttribute('aria-checked') === 'true') {
            this.singleton.player.playSample(this.audioCtx, this.singleton.player.sample_2);
        }
        if (this.pads[2].querySelectorAll('button')[this.currentNote].getAttribute('aria-checked') === 'true') {
            this.singleton.player.playSample(this.audioCtx, this.singleton.player.sample_3);
        }
        if (this.pads[3].querySelectorAll('button')[this.currentNote].getAttribute('aria-checked') === 'true') {
            this.singleton.player.playSample(this.audioCtx, this.singleton.player.sample_4);
        }
        if (this.pads[4].querySelectorAll('button')[this.currentNote].getAttribute('aria-checked') === 'true') {
            this.singleton.player.playSample(this.audioCtx, this.singleton.player.sample_5);
        }
        if (this.pads[5].querySelectorAll('button')[this.currentNote].getAttribute('aria-checked') === 'true') {
            this.singleton.player.playSample(this.audioCtx, this.singleton.player.sample_6);
        }
        if (this.pads[6].querySelectorAll('button')[this.currentNote].getAttribute('aria-checked') === 'true') {
            this.singleton.player.playSample(this.audioCtx, this.singleton.player.sample_7);
        }
        if (this.pads[7].querySelectorAll('button')[this.currentNote].getAttribute('aria-checked') === 'true') {
            this.singleton.player.playSample(this.audioCtx, this.singleton.player.sample_8);
        }
    }


    // while there are notes that will need to play before the next interval,
    // schedule them and advance the pointer.
    schedule() {
        while (this.nextNoteTime < this.singleton.scheduler.audioCtx.currentTime + this.scheduleAheadTime) {
            this.scheduleNote(this.currentNote, this.nextNoteTime);
            this.nextNote();
        }
        this.timerID = window.setTimeout(this.schedule.bind(this), this.lookahead);
    }

    // draw function to update the UI, so we can see when the beat progresses.
    draw() {
        // console.log("this in draw: ");
        // console.log(this);
        let drawNote = this.lastNoteDrawn;
        const currentTime = this.audioCtx.currentTime;
        while (this.notesInQueue.length && this.notesInQueue[0].time < currentTime) {
            drawNote = this.notesInQueue[0].note;
            this.notesInQueue.splice(0, 1);   // remove note from queue
        }
        // We only need to draw if the note has moved.
        if (this.lastNoteDrawn !== drawNote) {
            this.pads.forEach(el => {
                el.children[this.lastNoteDrawn].style.borderColor = 'hsla(0, 0%, 10%, 1)';
                el.children[drawNote].style.borderColor = 'hsla(49, 99%, 50%, 1)';
            });
            this.lastNoteDrawn = drawNote;
        }
        // set up to draw again
        requestAnimationFrame(this.draw.bind(this));
    }
}