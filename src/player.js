import FileHandler from './fileHandler.js';
import Scheduler from './scheduler.js';
import Singleton from './singleton.js';

/*
* class that takes care of storing and playing the audio files
*/
export default class Player {


    constructor(audioCtx) {

        this.sample_1 = null;
        this.sample_2 = null;
        this.sample_3 = null;
        this.sample_4 = null;
        this.sample_5 = null;
        this.sample_6 = null;
        this.sample_7 = null;
        this.sample_8 = null;

        this.audioCtx = audioCtx;

        this.singleton = new Singleton(audioCtx);
        this.fileHandler = this.singleton.fileHandler;

        this.scheduler = this.singleton.getScheduler();

        this.playButton = document.querySelector('[data-playing]');
        this.isPlaying = false;
    }// end constructor

    /**
     * event handler for play button which calls the scheduler to take care
     * of time-related tasks like playback
     * @param ev gets passed from main.js
     */
    eventHandler(ev) {
        // console.log("this in player.eventhandler(): ")
        // console.log(this);
        this.isPlaying = !this.isPlaying;
        if (this.isPlaying) { // start playing
            // check if context is in suspended state (autoplay policy)
            if (this.audioCtx.state === 'suspended') {
                this.audioCtx.resume();
            }
            this.currentNote = 0;
            this.nextNoteTime = this.audioCtx.currentTime;
            const schedule = this.singleton.scheduler.schedule.bind(this.singleton.scheduler);
            schedule();
            requestAnimationFrame(this.singleton.scheduler.draw.bind(this.singleton.scheduler)); // start the drawing loop.
            ev.target.dataset.playing = 'true';
        } else {
            window.clearTimeout(this.singleton.scheduler.timerID);
            ev.target.dataset.playing = 'false';
        }

    }

    // create a buffer, put in data, connect and play -> modify graph here if required
    playSample(audioContext, audioBuffer) {
        const sampleSource = audioContext.createBufferSource();
        sampleSource.buffer = audioBuffer;
        sampleSource.playbackRate.setValueAtTime(app.rate, this.audioCtx.currentTime);
        sampleSource.connect(audioContext.destination);
        sampleSource.start();
        return sampleSource;
    }

    // unused function that can be used later to add a noise generator as a track
    playNoise() {
        const bufferSize = this.audioCtx.sampleRate * this.noiseDuration; // set the time of the note
        const buffer = this.audioCtx.createBuffer(1, bufferSize, this.audioCtx.sampleRate); // create an empty buffer
        const data = buffer.getChannelData(0); // get data
        // fill the buffer with noise
        for (let i = 0; i < bufferSize; i++) {
            data[i] = Math.random() * 2 - 1;
        }
        // create a buffer source for our created data
        const noise = this.audioCtx.createBufferSource();
        noise.buffer = buffer;
        const bandpass = this.audioCtx.createBiquadFilter();
        bandpass.type = 'bandpass';
        bandpass.frequency.value = this.bandHz;
        // connect our graph
        noise.connect(bandpass).connect(this.audioCtx.destination);
        noise.start();
    }

}

